/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Tomas
 */
public class Teatteri {
    
    private Document doc;
    private HashMap<String, String> theatreMap;
    
    public HashMap<String, String> getTheatreMap(){
        return theatreMap;
    }
    
    
    public Teatteri(String ID, String date){
        String content = "";
        try{
            URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + ID + "&dt=" + date);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = br.readLine()) != null){
                content += line+"\n";
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            theatreMap = new HashMap();
            parseCurrentData("Show", "Title", "dttmShowStart", "dttmShowEnd");
           
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(TheatreAreas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData(String tag, String value1, String value2, String value3 ) {
        NodeList nodes = doc.getElementsByTagName(tag);
        for(int i = 1; i < nodes.getLength(); i++){
            
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {               
                theatreMap.put(Integer.toString(i) , getValue(value1, e)+"/"+getValue(value2, e)+"/"+getValue(value3, e));
            
            }else{
                 System.out.println("Error");
             }   
        }
    }
    
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
     public ArrayList getMovieList(){
        ArrayList<String> m = new ArrayList();
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 1; i < nodes.getLength(); i++){
             Node node = nodes.item(i);
             Element e = (Element) node;
             if (node.getNodeType() == Node.ELEMENT_NODE) {
                m.add(getValue("Title", e));
             }else{
                 System.out.println("Error");
             }   
        }
        return m;
    }
       
}
