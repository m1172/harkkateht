/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import org.xml.sax.SAXException;


/**
 *
 * 
 */
public class TheatreAreas {
    
    private Document doc;
    private HashMap<String, String> nameMap;
    
    public HashMap<String, String> getNameMap(){
        return nameMap;
    }
    
    public TheatreAreas() {
       String content = "";
        try{
            URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = br.readLine()) != null){
                content += line+"\n";
            }
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            
            doc.getDocumentElement().normalize();
            
            nameMap = new HashMap();
            parseCurrentData("TheatreArea", "Name", "ID");
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(TheatreAreas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    

    }
    
    private void parseCurrentData(String tag, String value1, String value2 ) {
        NodeList nodes = doc.getElementsByTagName(tag);
        
        for(int i = 1; i < nodes.getLength(); i++){
            
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {               
                nameMap.put(getValue(value1, e), getValue(value2, e));
            
            }else{
                 System.out.println("Error");
             }   
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    public ArrayList getTheatreList(){
        ArrayList<String> l = new ArrayList();
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        
        for(int i = 1; i < nodes.getLength(); i++){
             Node node = nodes.item(i);
             Element e = (Element) node;
             if (node.getNodeType() == Node.ELEMENT_NODE) {
                l.add(getValue("Name", e));
             }else{
                 System.out.println("Error");
             }   
        }
        return l;
    }
    
}
