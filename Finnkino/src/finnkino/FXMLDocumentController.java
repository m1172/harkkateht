/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finnkino;


import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * 
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button listButton;
    @FXML
    private ChoiceBox theatreBox;
    @FXML
    private TextField startField;
    @FXML
    private TextField dayField;
    @FXML
    private TextField endField;
    @FXML
    private TextField nameField;
    @FXML
    private Button nameButton;
    @FXML
    private TextArea textArea;
    
    TheatreAreas t = new TheatreAreas();
    ArrayList<String> teatterit = new ArrayList();
   
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
       textArea.clear();
       String ID = t.getNameMap().get(theatreBox.getValue());  // Halutun teatterin ID HashMapista, ottaa sisäänsä valitun teatterin nimen
       String date = "";
       
       
       if (dayField.getText().isEmpty()){
            Calendar cal = Calendar.getInstance();                  // Nukyinen päivä oikeassa muodossa
            SimpleDateFormat dateOnly = new SimpleDateFormat("dd.MM.yyyy");
            date = dateOnly.format(cal.getTime());           // Päivämäärä muutetaan merkkijonoksi
       } else{
           date = dayField.getText();
       }
       
       Teatteri m = new Teatteri(ID, date);                    // Luoodaan kyseinen teatteri ja sen ohjelmisto ID:n ja pvm avulla
       DateFormat datos1 = new SimpleDateFormat("hh:mm");
       DateFormat datos2 = new SimpleDateFormat("hh.mm");

       
       
       
       ArrayList <String> movies = m.getMovieList();    
       if (endField.getText().isEmpty()){
           endField.setText("24.00");
       }
       if (startField.getText().isEmpty()){
           startField.setText("00.00");
       }
       int i = 1;
       while (i < m.getTheatreMap().size()){
           String list[] = m.getTheatreMap().get(Integer.toString(i)).split("/");
           i++;
           String s[] = list[1].split("T");
           String e[] = list[2].split("T");
           try {
               Date scomp = datos1.parse(s[1]);
               Date start = datos2.parse(startField.getText());
               Date ecomp = datos1.parse(e[1]);
               Date end = datos2.parse(endField.getText());
               if ((start.before(scomp) || start.equals(scomp)) && (ecomp.before(end) || ecomp.equals(end))){
                   textArea.setText(textArea.getText() + list[0] + " " + s[1] + " - " + e[1] + "\n");
               } else{
                   textArea.setText(textArea.getText());
               }
           } catch (ParseException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       startField.clear();
       endField.clear();  
    }
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
        teatterit = t.getTheatreList();
       
        for (String i : teatterit){  // Lisätään kaikki teatterit valikkoon
            theatreBox.getItems().add(i);
        }
    }

    
    @FXML
    private void nameButtonAction(ActionEvent event) {
        Calendar cal = Calendar.getInstance();                  // Nukyinen päivä oikeassa muodossa
        SimpleDateFormat dateOnly = new SimpleDateFormat("dd.MM.yyyy");
        String date = dateOnly.format(cal.getTime());  
        String name = nameField.getText();
        teatterit = t.getTheatreList();
        textArea.clear();
        
        ArrayList <String> list = new ArrayList();
       
        for (String i : teatterit){
            String ID = t.getNameMap().get(i);
            Teatteri k = new Teatteri(ID, date);
            if(k.getMovieList().contains(name)){
                list.add(i);
            }
        }
        for (String j : list){
            textArea.setText(textArea.getText() + j + "\n");
        }
        

    }    
          
    
}
